Rails.application.routes.draw do
  devise_for :admins
  devise_for :users, controllers: {
    sessions: 'users/sessions'

    }
  devise_scope :admin do
    get "admins/users/list",
    to: "admins/sessions#list",
    as: "users_list"
  end

  resources :zombies do
    resources :brains

    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  end
  root to: 'zombies#index'
  get '/sexto/cerebros', to: 'brains#index', as: 'brains'
end