class Zombie < ApplicationRecord
    mount_uploader :avatar, ImageUploader
    has_many :brains, dependent: :destroy
    validates :name, presence: true
    validates :age, numericality: {only_integer: true, message: "Solo numeros enteros prro"}
    validates :bio, length: { in: 1..100 , message: "Maximo 100 caracteres prro"}
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, format: { :with => VALID_EMAIL_REGEX , message: "El formato del correo es invalido" }
    belongs_to :user
    
end
